import java.util.Scanner;
interface DiscountRate {
    double getServiceMemberDiscount();
    double getProductMemberDiscount();
}

class Customer implements DiscountRate {
    private String customerName;
    private String customerType;

    public Customer(String customerName, String customerType) {
        this.customerName = customerName;
        this.customerType = customerType;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    @Override
    public double getServiceMemberDiscount() {
        switch (customerType.toLowerCase()) {
            case "premium":
                return 0.20; // 20% discount
            case "gold":
                return 0.15; // 15% discount
            case "silver":
                return 0.10; // 10% discount
            default:
                return 0.0; // 0% discount
        }
    }

    @Override
    public double getProductMemberDiscount() {
        switch (customerType.toLowerCase()) {
            case "premium":
            case "gold":
            case "silver":
                return 0.10; // 10% discount for premium, gold, and silver
            default:
                return 0.0; // 0% discount for normal
        }
    }
}

class Sale {
    private Customer customer;
    private String date;
    private double serviceExpense;
    private double productExpense;

    public Sale(Customer customer, String date) {
        this.customer = customer;
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public double getServiceExpense() {
        return serviceExpense;
    }

    public void setServiceExpense(double serviceExpense) {
        this.serviceExpense = serviceExpense;
    }

    public double getProductExpense() {
        return productExpense;
    }

    public void setProductExpense(double productExpense) {
        this.productExpense = productExpense;
    }

    public double getTotalExpense() {
        double serviceDiscount = serviceExpense * customer.getServiceMemberDiscount();
        double productDiscount = productExpense * customer.getProductMemberDiscount();
        return serviceExpense - serviceDiscount + productExpense - productDiscount;
    }

    public void displayInfo() {
        System.out.println("Customer Name: " + customer.getCustomerName());
        System.out.println("Date: " + date);
        System.out.println("Service Expense: $" + serviceExpense);
        System.out.println("Product Expense: $" + productExpense);
        System.out.println("Total Expense: $" + getTotalExpense());
    }
}

public class Shoping {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Input customer details
        System.out.print("Enter customer name: ");
        String customerName = scanner.nextLine();
        System.out.print("Enter customer type (premium/gold/silver/normal): ");
        String customerType = scanner.nextLine();

        // Create customer instance
        Customer customer = new Customer(customerName, customerType);

        // Input sale details
        System.out.print("Enter sale date: ");
        String date = scanner.nextLine();
        System.out.print("Enter service expense: ");
        double serviceExpense = scanner.nextDouble();
        System.out.print("Enter product expense: ");
        double productExpense = scanner.nextDouble();

        // Create sale instance
        Sale sale = new Sale(customer, date);
        sale.setServiceExpense(serviceExpense);
        sale.setProductExpense(productExpense);

        // Display sale information
        System.out.println("\nSale Information:");
        sale.displayInfo();

        scanner.close();
    }
}
